package com.menya.calculator;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/*
 * @author a.minchekov
 */
public class MainActivity extends Activity {

	private String firstOperand;
	private TextView memElement;
	private String operation;
	private EditText inputText;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		inputText = (EditText) findViewById(R.id.editText1);
		memElement = (TextView)findViewById(R.id.textView1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public void saveInput(View view) {
		String inputValue = "";
		switch (view.getId()) {
			case R.id.num0:
				Button num0 = (Button) findViewById(R.id.num0);
				inputValue = num0.getText().toString();
				break;
			case R.id.num1:
				Button num1 = (Button) findViewById(R.id.num1);
				inputValue = num1.getText().toString();
				break;
			case R.id.num2:
				Button num2 = (Button) findViewById(R.id.num2);
				inputValue = num2.getText().toString();
				break;
			case R.id.num3:
				Button num3 = (Button) findViewById(R.id.num3);
				inputValue = num3.getText().toString();
				break;
			case R.id.num4:
				Button num4 = (Button) findViewById(R.id.num4);
				inputValue = num4.getText().toString();
				break;
			case R.id.num5:
				Button num5 = (Button) findViewById(R.id.num5);
				inputValue = num5.getText().toString();
				break;
			case R.id.num6:
				Button num6 = (Button) findViewById(R.id.num6);
				inputValue = num6.getText().toString();
				break;
			case R.id.num7:
				Button num7 = (Button) findViewById(R.id.num7);
				inputValue = num7.getText().toString();
				break;
			case R.id.num8:
				Button num8 = (Button) findViewById(R.id.num8);
				inputValue = num8.getText().toString();
				break;
			case R.id.num9:
				Button num9 = (Button) findViewById(R.id.num9);
				inputValue = num9.getText().toString();
				break;
			case R.id.point:
				Button point = (Button) findViewById(R.id.point);
				inputValue = point.getText().toString();
				break;
		}
		inputText.append(inputValue);
		
	}	
		public void sum(View view) {
			//get Operation value
			Button sum = (Button) findViewById(R.id.sum);
			operation = sum.getText().toString();
			firstOperand = inputText.getText().toString();
			memElement.setText(firstOperand + operation);
			inputText.setText("");
		}
		
		public void subtract(View view) {
			//get Operation value
			Button subtract = (Button) findViewById(R.id.subtract);
			operation = subtract.getText().toString();
			firstOperand = inputText.getText().toString();
			memElement.setText(firstOperand + operation);
			inputText.setText("");
		}
		
		public void multiply(View view) {
			//get Operation value
			Button multiply = (Button) findViewById(R.id.multiply);
			operation = multiply.getText().toString();
			firstOperand = inputText.getText().toString();
			memElement.setText(firstOperand + operation);
			inputText.setText("");
		}
		
		public void divide(View view) {
			//get Operation value
			Button divide = (Button) findViewById(R.id.divide);
			operation = divide.getText().toString();
			firstOperand = inputText.getText().toString();
			memElement.setText(firstOperand + operation);
			inputText.setText("");
		}
		
		public void equally(View view) {
			if (firstOperand != null) {
				double firstOperandValue;
				double secondOperandValue;
				double result;
				try { 
					secondOperandValue = Double.parseDouble(inputText.getText().toString());
					firstOperandValue = Double.parseDouble(firstOperand);
				} catch(NumberFormatException e) {
					Toast.makeText(this, "Please enter a valid operand",
				            Toast.LENGTH_LONG).show();
				        return;
				}
				if (operation.equals("+")) {
					result = firstOperandValue + secondOperandValue;
					inputText.setText(Double.toString(result));
					firstOperand = null;
					memElement.setText("");
					
				} else if (operation.equals("-")) {
					result = firstOperandValue - secondOperandValue;
					inputText.setText(Double.toString(result));
					firstOperand = null;
					memElement.setText("");
					
				} else if (operation.equals("*")) {
					result = firstOperandValue * secondOperandValue;
					inputText.setText(Double.toString(result));
					firstOperand = null;
					memElement.setText("");
					
				} else if (operation.equals("/")) {
					result = firstOperandValue / secondOperandValue;
					inputText.setText(Double.toString(result));
					firstOperand = null;
					memElement.setText("");
					
				}
			}
		}
		
		public void clear(View view) {
			//get Operation value
			//Button clear = (Button) findViewById(R.id.clear);
			operation = null;
			firstOperand = null;
			memElement.setText("");
			inputText.setText("");
		}
 }

